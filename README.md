# Visualizacion interactiva de casos de covid-19 en Argentina

Los datos se cargan manualmente en los csv ubicados en `data`, purgados al 29/03/2020. El procesamiento y generación de los gráficos se generan con jupyter notebooks que abajo se detallan.

- La visualización está disponible en: https://arcovid19.gitlab.io/viz/

aportes, contacto, acá: emiliano.lopez@gmail.com | @yosobreip

## Datos

Se usan dos archivos purgados manualmente y luego se generan `csv` para ser procesados:

- `arcovid19-provincias.ods` -> `arcovid19-SFe.csv`
- `totales_x_provincia_toViz.ods` -> `totales_x_provincia_toViz.csv`

Los siguientes archivos se usan para los datos mundiales, regularmente actualizados por RPaz en su repo:

- `mainlandchinacases.txt`
- `elsewhere.txt`
- `recovered.txt`
- `deaths.txt`
- `usa.txt`
- `sudamerica.txt`
- `arg.txt`

Los `csv` previos son usados en los notebooks para agregar columnas y generar los gráficos/mapas que a continuación se detallan:

## Notebooks

- `arcoviz.ipynb`: genero gráficos barras_provincias_anim y total_arg_provincias
- `geo-arcovid19.ipynb`: mapa provincia de Santa Fe
- `datos_mundiales.ipynb`: gráfico datos de totales_paises
- `variete.ipynb`: pruebas varias

## Generación de la web

`generate_index.py`: script que lee todos los htmls de los gráficos generados con los notebooks previos y los inyecta en el template `web/index_temporal.html`, solo queda renombrar a `index.html` manualmente (por ahora).

## CI/CD
Luego de cada push se copia automáticamente todo el directorio `web` a `public`

## Dependencias
- plotly
- pandas
- ...
