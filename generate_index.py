from string import Template

def clean_plotly_html(txt):
    """ clean firsts and last lines from a txt string

        - input: string with content from a plotly generated html
        - output: cleaned string
    """
    pass

# template de lectura y archivo de salida
index_output = open("web/index2.html", "w")
template_html = open("web/index_template3.html", "r").read()
template_tmp  = Template(template_html)

# figuras generadas con plotly y purgadas por ahora a mano -> nada mas que sacar el html y body
totales_paises         = open("graphs/totales_paises.html", "r").read()
barras_provincias_anim = open("graphs/barras_provincias_anim.html", "r").read()
total_arg_provincias   = open("graphs/total_arg_provincias.html", "r").read()
regiones               = open("graphs/regiones_sunburst.html", "r").read()
mapa_sfe               = open("graphs/mapa_sfe.html", "r").read()
#deptos_sfe             = open("graphs/deptos_sfe.html", "r").read()
sfe_detalle            = open("graphs/sfe_sunburst.html", "r").read()
sfe_evol_localidades   = open("graphs/sfe_evol_localidades.html", "r").read()
mapa_mundo_anim        = open("graphs/mapa_mundo_anim.html", "r").read()
#mapa_mundo_anim        = "<a href=mapa_mundo_anim.html>Ver animación mapa mundial</a>"
latam                  = open("graphs/latam.html", "r").read()



index_content = template_tmp.substitute(totales_paises          = totales_paises,
                                        barras_provincias_anim  = barras_provincias_anim,
                                        total_arg_provincias    = total_arg_provincias,
                                        regiones                = regiones,
                                        mapa_sfe                = mapa_sfe,
                                        deptos_sfe              = sfe_detalle,
                                        sfe_evol_localidades    = sfe_evol_localidades,
                                        mapa_mundo_anim         = mapa_mundo_anim,
                                        latam                   = latam,
                                        )

index_output.write(index_content)
index_output.close()